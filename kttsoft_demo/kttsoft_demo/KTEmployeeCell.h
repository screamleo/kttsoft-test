//
//  KTEmployeeCell.h
//  kttsoft_demo
//
//  Created by Leo Lashkevich on 7/14/15.
//  Copyright (c) 2015 Leo The Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KTEmployeeCell : UITableViewCell

@property (nonatomic, strong) NSString *nameStr;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, strong) NSString *department;

-(void)configureCell;
@end
