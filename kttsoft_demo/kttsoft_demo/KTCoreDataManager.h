//
//  KTCoreDataManager.h
//  kttsoft_demo
//
//  Created by Leo Lashkevich on 7/14/15.
//  Copyright (c) 2015 Leo The Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface KTCoreDataManager : NSObject
+(KTCoreDataManager*)sharedManager;
+(instancetype)alloc __attribute__((unavailable("alloc is unavailable, please use sharedManager")));
+(instancetype)new __attribute__((unavailable("New is unavailable, please use sharedManager")));
-(instancetype)init __attribute__((unavailable("init is unavailable, please use sharedManager")));
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(NSManagedObject*)objectForName:(NSString*)name;
@end
