//
//  AppDelegate.h
//  kttsoft_demo
//
//  Created by Leo Lashkevich on 7/14/15.
//  Copyright (c) 2015 Leo The Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

