//
//  KTProfileVC.m
//  kttsoft_demo
//
//  Created by Leo Lashkevich on 7/14/15.
//  Copyright (c) 2015 Leo The Developer. All rights reserved.
//

#import "KTProfileVC.h"
#import "KTCoreDataManager.h"
@interface KTProfileVC ()
@property (nonatomic, weak) IBOutlet UIScrollView *profileScrollView;
@property (nonatomic, weak) IBOutlet UITextField *nameField;
@property (nonatomic, weak) IBOutlet UILabel *dobLabel;
@property (nonatomic, weak) IBOutlet UILabel *ageLabel;
@property (nonatomic, weak) IBOutlet UITextField *positionField;
@property (nonatomic, weak) IBOutlet UILabel *departmentLabel;
@property (nonatomic, weak) IBOutlet UITextField *salaryField;
@property (nonatomic, weak) IBOutlet UIButton *editSaveButton;
@property (nonatomic, weak) IBOutlet UIButton *editDateButton;
@property (nonatomic, weak) IBOutlet UIButton *editDepartmentButton;
-(IBAction)editSaveButtonTapped:(id)sender;
-(IBAction)editDateButtonTapped:(id)sender;
-(IBAction)editDepartmentButtonTapped:(id)sender;
@property (nonatomic, strong) UIDatePicker *birthdatePicker;
@property (nonatomic, strong) UIPickerView *departmentPicker;
@property (nonatomic, strong) NSArray *departments;

@end

@implementation KTProfileVC
@synthesize birthdatePicker;
@synthesize departmentPicker;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"Profile"];
    self.navigationController.navigationBar.barTintColor = self.departmentColor;
    self.navigationController.navigationBar.translucent = YES;
    [[self.editSaveButton layer]setBorderColor:[UIColor blackColor].CGColor];
    [[self.editSaveButton layer]setBorderWidth:1.0];
    [self.editSaveButton setTitle:@"Save" forState:UIControlStateSelected];
    [self.editSaveButton setTitle:@"Edit" forState:UIControlStateNormal];
    self.nameField.delegate = self;
    self.positionField.delegate = self;
    self.salaryField.delegate = self;
    self.profileScrollView.delegate = self;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecieved:)];
    [tapGestureRecognizer setDelegate:self];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    birthdatePicker = [[UIDatePicker alloc]init];
    birthdatePicker.datePickerMode = UIDatePickerModeDate;
    //calculatind picker y point including height and future offset
    CGFloat yPointD = self.view.frame.size.height - 162;
    CGRect rectD = CGRectMake(0, yPointD, self.view.frame.size.width, 162);
    birthdatePicker.frame = rectD;
    [self.profileScrollView addSubview:birthdatePicker];
    [birthdatePicker setHidden:YES];
    [birthdatePicker setMaximumDate:[NSDate date]];
    [birthdatePicker addTarget:self action:@selector(dateChanged:)
     forControlEvents:UIControlEventValueChanged];
    self.departments = @[@"Business", @"Software", @"Design", @"Other"];
    departmentPicker = [[UIPickerView alloc]init];
    departmentPicker.frame = rectD;
    [self.profileScrollView addSubview:departmentPicker];
    [departmentPicker setHidden:YES];
    [departmentPicker setDelegate:self];




    
    
#pragma mark - Fetch person
    if (self.name) {
        NSManagedObject *person = [[KTCoreDataManager sharedManager] objectForName:self.name];
        self.nameField.text = [person valueForKey:@"name"];
        self.dobLabel.text = [self stringFromDateOfBirth:[person valueForKey:@"dob"]];
        self.ageLabel.text = [self calculateAgeStringFromDOB:[person valueForKey:@"dob"]];
        self.positionField.text = [person valueForKey:@"position"];
        self.departmentLabel.text = [person valueForKey:@"department"];
        self.salaryField.text = [NSString stringWithFormat:@"%@",[person valueForKey:@"salary"]];
    }else{
        [self editSaveButtonTapped:self.editSaveButton];
        self.nameField.text = @"";
        self.dobLabel.text = @"";
        self.ageLabel.text =@"";
        self.positionField.text = @"";
        self.departmentLabel.text = @"";
        self.salaryField.text = @"";
    }
    
}

-(NSString*)stringFromDateOfBirth:(NSDate*)date{
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc]init];
    [outputFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString* dateStr = [[NSString alloc] initWithFormat: @"%@",[outputFormatter stringFromDate:date]];
    return dateStr;

}
-(NSDate*)dateFromString:(NSString*)dateStr{
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"dd/MM/yyyy"];
    NSDate *formattedDate = [inputFormatter dateFromString:dateStr];
    return formattedDate;
}

-(NSString*)calculateAgeStringFromDOB:(NSDate*)dob{
    NSDate *todayDate = [NSDate date];
    int time = [todayDate timeIntervalSinceDate:dob];
    int allDays = (((time/60)/60)/24);
    int days = allDays%365;
    int years = (allDays-days)/365;
    NSString *agestr = [NSString stringWithFormat:@"%d", years];
    return agestr;
}

-(void)dateChanged:(id)sender{
    self.dobLabel.text = [self stringFromDateOfBirth:[birthdatePicker date]];
    self.ageLabel.text = [self calculateAgeStringFromDOB:[birthdatePicker date]];
}

-(void)editSaveButtonTapped:(id)sender{
    UIButton *bt = (UIButton*)sender;
    if (!bt.isSelected) {
        [bt setSelected:YES];
        [self.nameField setEnabled:YES];
        [self.positionField setEnabled:YES];
        [self.salaryField setEnabled:YES];
        [self.editDateButton setHidden:NO];
        [self.editDepartmentButton setHidden:NO];
    }else{
        [bt setSelected:NO];
        [self.nameField setEnabled:NO];
        [self.positionField setEnabled:NO];
        [self.salaryField setEnabled:NO];
        [self.editDateButton setHidden:YES];
        [self.editDepartmentButton setHidden:YES];
        if (self.name) {
            //If object already exists
            NSManagedObjectContext *moc = [KTCoreDataManager sharedManager].managedObjectContext;
            NSManagedObject *obj = [[KTCoreDataManager sharedManager]objectForName:self.name];
            [obj setValue:self.nameField.text forKey:@"name"];
            [obj setValue:self.positionField.text forKey:@"position"];
            [obj setValue:self.departmentLabel.text forKey:@"department"];
            NSDate *date = [self dateFromString:self.dobLabel.text];
            [obj setValue:date forKey:@"dob"];
            NSInteger salary = [self.salaryField.text integerValue];
            [obj setValue:@(salary) forKey:@"salary"];
            NSError* errorAdd = nil;
            if (![moc save:&errorAdd]) {
                NSLog(@"%@", [errorAdd localizedDescription]);
            }
        }else{
            //new object
            NSManagedObjectContext *moc = [KTCoreDataManager sharedManager].managedObjectContext;
            NSManagedObject* objectAdd = [NSEntityDescription insertNewObjectForEntityForName:@"Employee"
                                                                       inManagedObjectContext:moc];
            
            [objectAdd setValue:self.nameField.text forKey:@"name"];
            [objectAdd setValue:self.positionField.text forKey:@"position"];
            [objectAdd setValue:self.departmentLabel.text forKey:@"department"];
            NSDate *date = [self dateFromString:self.dobLabel.text];
            [objectAdd setValue:date forKey:@"dob"];
            NSInteger salary = [self.salaryField.text integerValue];
            [objectAdd setValue:@(salary) forKey:@"salary"];
            
            
            NSError* errorAdd = nil;
            if (![moc save:&errorAdd]) {
                NSLog(@"%@", [errorAdd localizedDescription]);
            }

        }
        
    }
}
-(void)tapRecieved:(UITapGestureRecognizer *)tapGestureRecognizer{
    [UIView animateWithDuration:0.3 animations:^{
        [self.profileScrollView setContentOffset:CGPointMake(0, 0)];
    }];
    [birthdatePicker setHidden:YES];
    [departmentPicker setHidden:YES];
    [self.nameField resignFirstResponder];
    [self.positionField resignFirstResponder];
    [self.salaryField resignFirstResponder];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [birthdatePicker setHidden:YES];
    if (textField == self.nameField) {
        [UIView animateWithDuration:0.3 animations:^{
            [self.profileScrollView setContentOffset:CGPointMake(0, 10)];
        }];
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            [self.profileScrollView setContentOffset:CGPointMake(0, 100)];
        }];
    }
    
    return YES;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isKindOfClass:[UIDatePicker class]]) {
        return NO; // ignore the touch
    }else if ([touch.view isKindOfClass:[UIPickerView class]]){
        return NO;
    }
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [UIView animateWithDuration:0.3 animations:^{
        [self.profileScrollView setContentOffset:CGPointMake(0, 0)];
    }];
    [textField resignFirstResponder];
    return YES;
}

-(void)editDateButtonTapped:(id)sender{
    [UIView animateWithDuration:0.3 animations:^{
        [self.profileScrollView setContentOffset:CGPointMake(0, 50)];
        [birthdatePicker setHidden:NO];
        [departmentPicker setHidden:YES];
    }];

}

-(void)editDepartmentButtonTapped:(id)sender{
    [UIView animateWithDuration:0.3 animations:^{
        [self.profileScrollView setContentOffset:CGPointMake(0, 150)];
        [birthdatePicker setHidden:YES];
        [departmentPicker setHidden:NO];
    }];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.x != 0) {
        CGPoint offset = scrollView.contentOffset;
        offset.x = 0;
        scrollView.contentOffset = offset;
    }
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return _departments.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return self.departments[row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.departmentLabel.text = self.departments[row];
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
@end
