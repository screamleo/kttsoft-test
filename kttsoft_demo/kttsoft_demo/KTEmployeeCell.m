//
//  KTEmployeeCell.m
//  kttsoft_demo
//
//  Created by Leo Lashkevich on 7/14/15.
//  Copyright (c) 2015 Leo The Developer. All rights reserved.
//
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.f green:g/255.f blue:b/255.f alpha:a/255.f]
#import "KTEmployeeCell.h"
@interface KTEmployeeCell ()
@property (nonatomic, weak) IBOutlet UILabel *name;
@property (nonatomic, weak) IBOutlet UILabel *workplace;
@property (nonatomic, strong) UIColor *departmentColor;
@end
@implementation KTEmployeeCell

- (void)awakeFromNib {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell{
    self.name.text = self.nameStr;
    NSString *workplaceStr = [[NSString alloc]initWithFormat:@"%@, %@ dep.", self.position, self.department];
    self.workplace.text = workplaceStr;
    if ([self.department isEqualToString:@"Software"]) {
        self.departmentColor = RGBA(133, 224, 242, 200);
    }else if ([self.department isEqualToString:@"Design"]){
        self.departmentColor = RGBA(177, 76, 245, 200);
    }else if ([self.department isEqualToString:@"Business"]){
        self.departmentColor = RGBA(87, 245, 76, 200);
    }else if ([self.department isEqualToString:@"Support"]){
        self.departmentColor = RGBA(255, 255, 153, 200);
    }else{
        self.departmentColor = RGBA(255, 102, 102, 200);
    }
    self.backgroundColor = self.departmentColor;
}

@end
