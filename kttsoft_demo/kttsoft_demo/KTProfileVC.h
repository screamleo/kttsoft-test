//
//  KTProfileVC.h
//  kttsoft_demo
//
//  Created by Leo Lashkevich on 7/14/15.
//  Copyright (c) 2015 Leo The Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KTProfileVC : UIViewController <UITextFieldDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) UIColor *departmentColor;

@end
