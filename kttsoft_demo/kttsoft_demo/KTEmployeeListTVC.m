//
//  KTEmployeeListTVC.m
//  
//
//  Created by Leo Lashkevich on 7/14/15.
//
//

#import "KTEmployeeListTVC.h"
#import "KTEmployeeCell.h"
#import "KTCoreDataManager.h"
#import "KTProfileVC.h"
@interface KTEmployeeListTVC ()
@property (nonatomic, strong)NSArray *employeesArray;
@end

@implementation KTEmployeeListTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationItem setTitle:@"List"];
    
    UIBarButtonItem *bt = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addEmployee:)];
    self.navigationItem.rightBarButtonItem = (UIBarButtonItem*)bt;
    [self standardSort];
    
       UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0;
    lpgr.delegate = self;
    [self.tableView addGestureRecognizer:lpgr];
}

-(void)viewDidAppear:(BOOL)animated{
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    [self standardSort];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.employeesArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KTEmployeeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"KTEmployeeCell" forIndexPath:indexPath];
    NSManagedObject *object = [self.employeesArray objectAtIndex:indexPath.row];
    cell.nameStr = [object valueForKey:@"name"];
    cell.position = [object valueForKey:@"position"];
    cell.department = [object valueForKey:@"department"];
    [cell configureCell];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    KTEmployeeCell *cell = (KTEmployeeCell*)[tableView cellForRowAtIndexPath:indexPath];
    NSString *fullName = cell.nameStr;
    KTProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"KTProfileVC"];
    vc.name = fullName;
    vc.departmentColor = cell.backgroundColor;
    [self.navigationController showViewController:vc sender:self];
}

-(void)addEmployee:(id)sender{
    KTProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"KTProfileVC"];
    vc.name = nil;
    [self.navigationController showViewController:vc sender:self];
}
-(void)standardSort{
    NSManagedObjectContext *object = [KTCoreDataManager sharedManager].managedObjectContext;
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    fetch.entity = [NSEntityDescription entityForName:@"Employee" inManagedObjectContext:object];
    NSSortDescriptor *sortDescr = [[NSSortDescriptor alloc]initWithKey:@"name" ascending:NO];
    [fetch setSortDescriptors:@[sortDescr]];
    self.employeesArray = [object executeFetchRequest:fetch error:nil];
    [self.tableView reloadData];
    self.navigationItem.leftBarButtonItem = nil;

}

-(void)handleLongPress:(UILongPressGestureRecognizer*)lpgr{
    CGPoint point = [lpgr locationInView:self.tableView];
    NSIndexPath *path= [self.tableView indexPathForRowAtPoint:point];
    KTEmployeeCell *cell = (KTEmployeeCell*)[self.tableView cellForRowAtIndexPath:path];
    if (!cell) {
        return;
    }
    NSString *depart = cell.department;
    NSManagedObjectContext *object = [KTCoreDataManager sharedManager].managedObjectContext;
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    fetch.entity = [NSEntityDescription entityForName:@"Employee" inManagedObjectContext:object];
    NSSortDescriptor *sortDescr = [[NSSortDescriptor alloc]initWithKey:@"department" ascending:NO];
    [fetch setSortDescriptors:@[sortDescr]];
    fetch.predicate = [NSPredicate predicateWithFormat:@"department == %@", depart];
    self.employeesArray = [object executeFetchRequest:fetch error:nil];
    [self.tableView reloadData];
    UIBarButtonItem *bt = [[UIBarButtonItem alloc]initWithTitle:@"Reset" style:UIBarButtonItemStylePlain target:self action:@selector(standardSort)];
    self.navigationItem.leftBarButtonItem = (UIBarButtonItem*)bt;
}

@end
