//
//  AppDelegate.m
//  kttsoft_demo
//
//  Created by Leo Lashkevich on 7/14/15.
//  Copyright (c) 2015 Leo The Developer. All rights reserved.
//

#import "AppDelegate.h"
#import "KTCoreDataManager.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    BOOL launchedBefore = [[NSUserDefaults standardUserDefaults] boolForKey:@"launchedBefore"];
    if (!launchedBefore) {
        [self addDefaultEmployees];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"launchedBefore"];
    }
    return YES;
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    
}

-(void)addDefaultEmployees{
    if (![[NSThread currentThread] isMainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self addDefaultEmployees];
        });
        return ;
    }
    NSString *path = [[NSBundle mainBundle]pathForResource:@"DefaultEmployees" ofType:@"plist"];
    NSDictionary *properties = [[NSDictionary alloc] initWithContentsOfFile:path];
    NSArray *names = (NSArray*)[properties valueForKey:@"names"];
    NSArray *postitons = (NSArray*)[properties valueForKey:@"positions"];
    NSArray *departments = (NSArray*)[properties valueForKey:@"departments"];
    for (int i = 0; i < names.count; i++) {
        
    
        NSManagedObjectContext *moc = [KTCoreDataManager sharedManager].managedObjectContext;
        NSManagedObject* objectAdd = [NSEntityDescription insertNewObjectForEntityForName:@"Employee"
                                                                   inManagedObjectContext:moc];
        
        [objectAdd setValue:[names objectAtIndex:i] forKey:@"name"];
        [objectAdd setValue:[postitons objectAtIndex:i] forKey:@"position"];
        [objectAdd setValue:[departments objectAtIndex:i] forKey:@"department"];
        NSDate *dob = [NSDate dateWithTimeIntervalSince1970:0];
        [objectAdd setValue:dob forKey:@"dob"];
        

        NSError* errorAdd = nil;
        if (![moc save:&errorAdd]) {
            NSLog(@"%@", [errorAdd localizedDescription]);
        }

    }
}


@end
